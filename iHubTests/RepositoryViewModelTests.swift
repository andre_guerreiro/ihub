import XCTest
@testable import iHub

final class RepositoryViewModelTests: XCTestCase {

    // MARK: Test Init
    
    func test_init_withDescription() throws {
        let repository = Repository(
            name: "Hello-World",
            description: "This your first repo!",
            creationDate: Date(timeIntervalSince1970: 1296048203),
            stargazersCount: 80
        )
        
        let repositoryViewModel = RepositoryViewModel(repository: repository)
        
        XCTAssertEqual(repositoryViewModel.name, "Hello-World")
        XCTAssertEqual(repositoryViewModel.description, "This your first repo!")
        XCTAssertEqual(repositoryViewModel.creationDate, "2011-01-26")
        XCTAssertEqual(repositoryViewModel.stargazersCount, "80")
    }
    
    func test_init_withoutDescription() throws {
        let repository = Repository(
            name: "Hello-World",
            description: nil,
            creationDate: Date(timeIntervalSince1970: 1296048203),
            stargazersCount: 80
        )
        
        let repositoryViewModel = RepositoryViewModel(repository: repository)
        
        XCTAssertEqual(repositoryViewModel.name, "Hello-World")
        XCTAssertNil(repositoryViewModel.description)
        XCTAssertEqual(repositoryViewModel.creationDate, "2011-01-26")
        XCTAssertEqual(repositoryViewModel.stargazersCount, "80")
    }
}
