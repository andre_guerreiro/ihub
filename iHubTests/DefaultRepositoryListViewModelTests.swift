import XCTest
@testable import iHub

final class DefaultRepositoryListViewModelTests: XCTestCase {
    
    var cancelBag = CancelBag()

    // MARK: -
    
    func test_initalPartialSequenceIsLoaded_afterInit() throws {
        let mockSequence = RepositoryListMock.makeMockSequence()
        let gitHubAPI = MockGitHubAPI(mockSequence: mockSequence, responseDelay: 0)
        let viewModel = DefaultRepositoryListViewModel(gitHubAPI: gitHubAPI)
        
        let expectation = XCTestExpectation(description: #function)
        let expectedResult = mockSequence[0].value!.value.map { RepositoryViewModel(repository: $0) }
        
        viewModel
            .repositories
            .filter { $0.loaded }
            .sink { result in
                switch result {
                case .loaded(.partial(expectedResult)):
                    expectation.fulfill()
                default:
                    XCTFail()
                }
            }
            .store(in: cancelBag)
            
        wait(for: [expectation], timeout: 1)
    }
    
    func test_willDisplayItem_loadsCorrespondingPartialSequence() throws {
        let pageSize = 30
        let displayIndex = 29
        
        let mockSequence = RepositoryListMock.makeMockSequence(pageSize: pageSize)
        let gitHubAPI = MockGitHubAPI(mockSequence: mockSequence, responseDelay: 0)
        let viewModel = DefaultRepositoryListViewModel(gitHubAPI: gitHubAPI)
        
        let expectation = XCTestExpectation(description: #function)
        let initialResult = mockSequence[0].value!.value.map { RepositoryViewModel(repository: $0) }
        let expectedResult = mockSequence[1].value!.value.map { RepositoryViewModel(repository: $0) }
        
        let loadedRepositories = viewModel
            .repositories
            .filter { $0.loaded }
        
        loadedRepositories
            .first()
            .sink { result in
                switch result {
                case .loaded(.partial(initialResult)):
                    viewModel.willDisplayItem(forIndex: displayIndex)
                default:
                    XCTFail()
                }
            }
            .store(in: cancelBag)
        
        loadedRepositories
            .dropFirst()
            .sink { result in
                switch result {
                case .loaded(.partial(expectedResult)):
                    expectation.fulfill()
                default:
                    XCTFail()
                }
            }
            .store(in: cancelBag)
            
        wait(for: [expectation], timeout: 2)
    }
}
