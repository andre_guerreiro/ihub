import Foundation
import Combine

#if DEBUG

enum RepositoryListMock {
    
    static let appleReposJSONName = "AppleRepos"
        
    static func makeMockSequence(pageSize: Int = 30) -> [Loadable<PartialSequence<[Repository]>>] {
        let fileURL = Bundle.main.url(forResource: appleReposJSONName, withExtension: "json")!
        let data = try! Data(contentsOf: fileURL)
        
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        
        let parsedSortedRepositories = try! decoder
            .decode([Repository].self, from: data)
            .sorted(by: \.name)
                
        let numberOfPages = (parsedSortedRepositories.count - 1) / pageSize + 1
        
        return (1...numberOfPages)
            .map { page in
                let repositories = Array(parsedSortedRepositories.prefix(page * pageSize))
                return page == numberOfPages ? .complete(repositories) : .partial(repositories)
            }
            .map { .loaded($0) }
    }
}

#endif
