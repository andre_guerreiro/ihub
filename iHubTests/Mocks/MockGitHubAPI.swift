import Foundation
import Combine

#if DEBUG

final class MockGitHubAPI: GitHubAPI {
    
    // MARK: Properties
    
    let mockSequence: [Loadable<PartialSequence<[Repository]>>]
    let responseDelay: TimeInterval
    
    private var pointer: Int = 0
    
    // MARK: Init
    
    init(mockSequence: [Loadable<PartialSequence<[Repository]>>], responseDelay: TimeInterval = 1) {
        self.mockSequence = mockSequence
        self.responseDelay = responseDelay
    }
    
    // MARK: GitHubAPI
    
    func loadSortedRepositories(forOrganization organization: String, beyondIndex index: Int?) -> AnyPublisher<Loadable<PartialSequence<[Repository]>>, Never> {
        
        assert(pointer < mockSequence.count, "Not enough elements in mockSequence!")
        
        let mockPublisher = Just(mockSequence[pointer])
            .prepend(.isLoading)
        
        pointer += 1
        
        return Timer
            .publish(every: responseDelay, on: RunLoop.main, in: .default)
            .autoconnect()
            .prepend(Date())
            .zip(mockPublisher)
            .map { $1 }
            .eraseToAnyPublisher()
    }
}

#endif
