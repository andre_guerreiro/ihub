import Foundation

struct RepositoryViewModel: Hashable {
    var name: String
    var description: String?
    var creationDate: String
    var stargazersCount: String
    
    // MARK: Init
    
    init(repository: Repository) {
        name = repository.name
        description = repository.description
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        creationDate = dateFormatter.string(from: repository.creationDate)
        
        stargazersCount = String(repository.stargazersCount)
    }
}
