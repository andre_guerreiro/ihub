import Foundation
import Combine

private enum Constants {
    static let titleSuffix = "Repositories"
}

// MARK: -

final class DefaultRepositoryListViewModel: RepositoryListViewModel {

    // MARK: Outputs

    var title: AnyPublisher<String, Never>
    let loadingCellText = "Loading more..."
    var repositories: AnyPublisher<Loadable<PartialSequence<[RepositoryViewModel]>>, Never> {
        repositoriesSubject
            .removeDuplicates()
            .eraseToAnyPublisher()
    }
    
    // MARK: Inputs
    
    func willDisplayItem(forIndex index: Int) {
        switch repositoriesSubject.value {
        case .notRequested:
            loadRepositories(beyondIndex: index)
        case let .loaded(.partial(currentValue)) where index >= currentValue.count - 1:
            loadRepositories(beyondIndex: index)
        default:
            return
        }
    }

    // MARK: Properties

    private let gitHubAPI: GitHubAPI
    private let organizationName = "apple"
    
    private let repositoriesSubject = CurrentValueSubject<Loadable<PartialSequence<[RepositoryViewModel]>>, Never>(.notRequested)
    private let cancelBag = CancelBag()

    // MARK: Init

    init(gitHubAPI: GitHubAPI) {
        self.gitHubAPI = gitHubAPI
        
        title = Just(organizationName)
            .map { organizationName in organizationName.capitalizingFirstLetter() + " " + Constants.titleSuffix }
            .eraseToAnyPublisher()
        
        loadRepositories()
    }
    
    // MARK: Repository Loading
    
    private func loadRepositories(beyondIndex index: Int? = nil) {
        gitHubAPI
            .loadSortedRepositories(forOrganization: organizationName, beyondIndex: index)
            .map { loadable in
                loadable.map { partialSequence in
                    partialSequence.map(RepositoryViewModel.init(repository:))
                }
            }
            .sink { [weak self] repositories in
                self?.repositoriesSubject.send(repositories)
            }
            .store(in: cancelBag)
    }
}
