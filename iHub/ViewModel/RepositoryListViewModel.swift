import Foundation
import Combine

protocol RepositoryListViewModel {
    
    // MARK: Outputs
    
    var title: AnyPublisher<String, Never> { get }
    var loadingCellText: String { get }
    var repositories: AnyPublisher<Loadable<PartialSequence<[RepositoryViewModel]>>, Never> { get }
    
    // MARK: Inputs
    
    func willDisplayItem(forIndex index: Int)
}
