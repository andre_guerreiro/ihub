import Foundation

struct Repository {
    let name: String
    let description: String?
    let creationDate: Date
    let stargazersCount: Int
}

// MARK: Decodable

extension Repository: Decodable {
    enum CodingKeys: String, CodingKey {
        case name
        case description
        case creationDate = "created_at"
        case stargazersCount = "stargazers_count"
    }
}
