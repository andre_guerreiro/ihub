import UIKit

final class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    // MARK: Properties
    
    var window: UIWindow?
    
    // MARK: UISceneDelegate

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        let appEnvironment = AppEnvironment.bootstrap()

        let viewModel = DefaultRepositoryListViewModel(gitHubAPI: appEnvironment.apis.gitHubAPI)
        let repositoryListViewController = RepositoryListViewController(viewModel: viewModel)
        
        let navigationController = UINavigationController(rootViewController: repositoryListViewController)
        navigationController.navigationBar.prefersLargeTitles = true
        
        let window = UIWindow(windowScene: windowScene)
        window.rootViewController = navigationController
        self.window = window
        window.makeKeyAndVisible()
    }
}
