import UIKit

enum Theme {
    enum Spacing {
        static let standard: CGFloat = 16
        static let small: CGFloat = 8
        static let extraSmall: CGFloat = 4
    }
    
    enum Colors {
        static let main = UIColor.systemBlue
        static let alternative = UIColor.systemGray
        static let star = UIColor.systemYellow
    }
    
    enum Fonts {
        static let title = UIFont.systemFont(ofSize: 22)
        static let standard = UIFont.systemFont(ofSize: 14)
        static let small = UIFont.systemFont(ofSize: 12)
    }
    
    enum Icons {
        static let star = "star.fill"
    }
}
