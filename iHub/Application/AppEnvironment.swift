import Foundation

struct AppEnvironment {
    let apis: APIs
}

extension AppEnvironment {
    struct APIs {
        let gitHubAPI: GitHubAPI
    }
}

// MARK: Bootstrapping

extension AppEnvironment {
    
    static func bootstrap() -> AppEnvironment {
        let session = configuredURLSession()
        let decoder = configuredJSONDecoder()
        let apis = configuredAPIs(session: session, jsonDecoder: decoder)
        
        return AppEnvironment(apis: apis)
    }
    
    // MARK: URLSession
    
    private static func configuredURLSession() -> URLSession {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 20
        configuration.timeoutIntervalForResource = 30
        configuration.waitsForConnectivity = true
        return URLSession(configuration: configuration)
    }
    
    // MARK: JSONDecoder
    
    private static func configuredJSONDecoder() -> JSONDecoder {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        return decoder
    }
    
    // MARK: APIs
    
    private static func configuredAPIs(session: URLSession, jsonDecoder: JSONDecoder) -> AppEnvironment.APIs {
        let gitHubAPI = DefaultGitHubAPI(
            baseURL: "https://api.github.com",
            session: session,
            jsonDecoder: jsonDecoder
        )

        return .init(gitHubAPI: gitHubAPI)
    }
}
