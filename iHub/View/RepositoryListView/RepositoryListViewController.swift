import UIKit
import Combine

extension RepositoryListViewController {
    enum Section {
        case repositories
        case loading
    }
}

// MARK: -

final class RepositoryListViewController: UIViewController {
    
    // MARK: Properties
    
    private var viewModel: RepositoryListViewModel
    private lazy var dataSource = { RepositoryListDataSource(collectionView: collectionView) }()
    private let cancelBag = CancelBag()
    
    // MARK: Views
    
    private lazy var collectionView: UICollectionView = {
        let configuration = UICollectionLayoutListConfiguration(appearance: .plain)
        let layout = UICollectionViewCompositionalLayout.list(using: configuration)
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.delegate = self
        return collectionView
    }()
    
    private let activityIndicatorView = UIActivityIndicatorView(style: .large)
    
    private let errorMessageLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    // MARK: Init
    
    init(viewModel: RepositoryListViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @available(*, unavailable)
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemBackground
        
        setUpBindings()
        layoutViews()
    }
    
    // MARK: Bindings
    
    private func setUpBindings() {
        [
            viewModel
                .title
                .sink { [weak self] title in
                    self?.title = title
                },
            
            viewModel
                .repositories
                .unwrappedValues()
                .map(RepositoryListDataSource.makeSnapshotProvider(loadingCellText: viewModel.loadingCellText))
                .sink { [weak self] snapshot in
                    self?.dataSource.apply(snapshot)
                },
            
            viewModel
                .repositories
                .unwrappedLoadingStates()
                .sink { [weak self] isLoading in
                    guard let self = self else { return }
                    
                    if isLoading && self.dataSource.numberOfSections(in: self.collectionView) == 0 {
                        self.activityIndicatorView.startAnimating()
                    } else {
                        self.activityIndicatorView.stopAnimating()
                    }
                },
            
            viewModel
                .repositories
                .unwrappedErrors()
                .sink { [weak self] error in
                    self?.dataSource.apply(RepositoryListDataSource.Snapshot())
                    self?.errorMessageLabel.text = error.localizedDescription
                }
        ]
        .store(in: cancelBag)
    }
    
    // MARK: View Layout
    
    private func layoutViews() {
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(collectionView)
        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityIndicatorView)
        NSLayoutConstraint.activate([
            activityIndicatorView.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
            activityIndicatorView.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor)
        ])
        
        errorMessageLabel.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(errorMessageLabel)
        NSLayoutConstraint.activate([
            errorMessageLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: Theme.Spacing.standard),
            errorMessageLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -Theme.Spacing.standard),
            errorMessageLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: Theme.Spacing.standard),
            errorMessageLabel.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -Theme.Spacing.standard)
        ])
    }
}

// MARK: - UICollectionViewDelegate

extension RepositoryListViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        viewModel.willDisplayItem(forIndex: indexPath.row)
    }
}
