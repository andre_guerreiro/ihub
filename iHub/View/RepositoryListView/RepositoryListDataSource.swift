import UIKit

enum RepositoryListDataSourceItem: Hashable {
    case repository(RepositoryViewModel)
    case loading(text: String)
}

// MARK: -

typealias RepositoryListDataSource = UICollectionViewDiffableDataSource<RepositoryListViewController.Section, RepositoryListDataSourceItem>

extension RepositoryListDataSource {
    typealias Snapshot = NSDiffableDataSourceSnapshot<RepositoryListViewController.Section, RepositoryListDataSourceItem>
    
    // MARK: Providers
    
    private static func makeCellProvider() -> CellProvider {
        let repositoryCellRegistration = UICollectionView.CellRegistration<UICollectionViewCell, RepositoryViewModel> { cell, _, repositoryViewModel in
            cell.contentConfiguration = RepositoryContentConfiguration(repositoryViewModel: repositoryViewModel)
        }
        let loadingCellRegistration = UICollectionView.CellRegistration<UICollectionViewCell, String> { cell, _, loadingText in
            cell.contentConfiguration = LoadingContentConfiguration(text: loadingText)
        }
        return { collectionView, indexPath, item in
            switch item {
            case let .repository(repositoryViewModel):
                return collectionView.dequeueConfiguredReusableCell(using: repositoryCellRegistration, for: indexPath, item: repositoryViewModel)
            case let .loading(loadingText):
                return collectionView.dequeueConfiguredReusableCell(using: loadingCellRegistration, for: indexPath, item: loadingText)
            }            
        }
    }
    
    static func makeSnapshotProvider(loadingCellText: String) -> (PartialSequence<[RepositoryViewModel]>) -> Snapshot {
        return { repositoryViewModels in
            var snapshot = Snapshot()
            snapshot.appendSections([.repositories])
            snapshot.appendItems(repositoryViewModels.value.map { .repository($0) })
            if !repositoryViewModels.isComplete {
                snapshot.appendSections([.loading])
                snapshot.appendItems([.loading(text: loadingCellText)])
            }
            return snapshot
        }
    }
    
    // MARK: Init
    
    convenience init(collectionView: UICollectionView) {
        self.init(collectionView: collectionView, cellProvider: Self.makeCellProvider())
    }
}
