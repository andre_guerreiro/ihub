import UIKit

final class RepositoryContentView: UIView, UIContentView {
    
    // MARK: Properties
    
    var configuration: UIContentConfiguration {
        didSet {
            guard oldValue as? RepositoryContentConfiguration != configuration as? RepositoryContentConfiguration else { return }
            configure(configuration)
        }
    }
    
    // MARK: Views
    
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.font = Theme.Fonts.title
        label.textColor = Theme.Colors.main
        return label
    }()
    
    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = Theme.Fonts.standard
        label.numberOfLines = 0
        return label
    }()
    
    private let creationDateLabel: UILabel = {
        let label = UILabel()
        label.font = Theme.Fonts.small
        label.textColor = Theme.Colors.alternative
        return label
    }()
    
    private let stargazersCountLabel: UILabel = {
        let label = UILabel()
        label.font = Theme.Fonts.small
        label.textColor = Theme.Colors.alternative
        return label
    }()
    
    private let stargazersCountIcon: UIImageView = {
        let configuration = UIImage.SymbolConfiguration(font: Theme.Fonts.small)
        let image = UIImage(systemName: Theme.Icons.star, withConfiguration: configuration)
        let imageView = UIImageView(image: image)
        imageView.tintColor = Theme.Colors.star
        return imageView
    }()
    
    // MARK: Init
    
    init(configuration: UIContentConfiguration) {
        self.configuration = configuration
        
        super.init(frame: .zero)
        
        layoutViews()
        configure(configuration)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: View Layout
    
    private func layoutViews() {
        let detailsStackView = UIStackView(arrangedSubviews: [creationDateLabel, stargazersCountLabel, stargazersCountIcon])
        detailsStackView.spacing = Theme.Spacing.extraSmall
        creationDateLabel.setContentHuggingPriority(.defaultLow, for: .horizontal)
        stargazersCountLabel.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        stargazersCountIcon.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        
        let contentStackView = UIStackView(arrangedSubviews: [nameLabel, descriptionLabel, detailsStackView])
        contentStackView.axis = .vertical
        contentStackView.spacing = Theme.Spacing.small
        addSubview(contentStackView)
        contentStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            contentStackView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: Theme.Spacing.standard),
            contentStackView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -Theme.Spacing.standard),
            contentStackView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: Theme.Spacing.small),
            contentStackView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -Theme.Spacing.small)
        ])
    }
    
    // MARK: Configuration
    
    private func configure(_ configuration: UIContentConfiguration) {
        guard let configuration = configuration as? RepositoryContentConfiguration else { return }
        
        nameLabel.text = configuration.name
        descriptionLabel.text = configuration.description
        creationDateLabel.text = configuration.creationDate
        stargazersCountLabel.text = configuration.stargazersCount
    }
}
