import UIKit

struct RepositoryContentConfiguration: UIContentConfiguration {
    
    // MARK: Properties
    
    var name: String
    var description: String?
    var creationDate: String
    var stargazersCount: String
    
    // MARK: UIContentConfiguration
    
    func makeContentView() -> UIView & UIContentView {
        return RepositoryContentView(configuration: self)
    }
    
    func updated(for state: UIConfigurationState) -> RepositoryContentConfiguration {
        return self
    }
}

// MARK: - Convenience Init

extension RepositoryContentConfiguration {
    init(repositoryViewModel: RepositoryViewModel) {
        name = repositoryViewModel.name
        description = repositoryViewModel.description
        creationDate = repositoryViewModel.creationDate
        stargazersCount = repositoryViewModel.stargazersCount
    }
}

// MARK: Equatable

extension RepositoryContentConfiguration: Equatable {}
