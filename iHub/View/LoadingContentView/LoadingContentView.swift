import UIKit

final class LoadingContentView: UIView, UIContentView {
    
    // MARK: Properties
    
    var configuration: UIContentConfiguration {
        didSet {
            guard oldValue as? LoadingContentConfiguration != configuration as? LoadingContentConfiguration else { return }
            configure(configuration)
        }
    }
    
    // MARK: Views
    
    private let textLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }()
    
    // MARK: Init
    
    init(configuration: UIContentConfiguration) {
        self.configuration = configuration
        
        super.init(frame: .zero)
        
        layoutViews()
        configure(configuration)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: View Layout
    
    private func layoutViews() {
        addSubview(textLabel)
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            textLabel.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: Theme.Spacing.standard),
            textLabel.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -Theme.Spacing.standard),
            textLabel.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: Theme.Spacing.standard),
            textLabel.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -Theme.Spacing.standard)
        ])
    }
    
    // MARK: Configuration
    
    private func configure(_ configuration: UIContentConfiguration) {
        guard let configuration = configuration as? LoadingContentConfiguration else { return }
        
        textLabel.text = configuration.text
    }
}
