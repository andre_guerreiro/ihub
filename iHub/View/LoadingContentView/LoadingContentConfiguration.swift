import UIKit

struct LoadingContentConfiguration: UIContentConfiguration {
    
    // MARK: Properties
    
    var text: String
    
    // MARK: UIContentConfiguration
    
    func makeContentView() -> UIView & UIContentView {
        return LoadingContentView(configuration: self)
    }
    
    func updated(for state: UIConfigurationState) -> LoadingContentConfiguration {
        return self
    }
}

// MARK: Equatable

extension LoadingContentConfiguration: Equatable {}
