import Foundation

protocol Endpoint {
    var method: HTTPMethod { get }
    var headers: [String: String]? { get }
    var path: String { get }
    var parameters: [URLQueryItemConvertible]? { get }
    var body: Data? { get }
}

// MARK: URLRequest Creation

extension Endpoint {
    func urlRequest(forBaseURL baseURL: String) throws -> URLRequest {
        guard let baseURL = URL(string: baseURL),
              let url = urlComponents.url(relativeTo: baseURL)
        else { throw NetworkError.invalidURL }
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = headers
        request.httpBody = body
        
        return request
    }
    
    private var urlComponents: URLComponents {
        var urlComponents = URLComponents()
        urlComponents.path = path
        urlComponents.queryItems = parameters.map { $0.map { $0.queryItem } }
        return urlComponents
    }
}
