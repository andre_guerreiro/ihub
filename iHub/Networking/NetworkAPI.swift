import Foundation
import Combine

protocol NetworkAPI {
    var baseURL: String { get }
    var session: URLSession { get }
    var jsonDecoder: JSONDecoder { get }
}

// MARK: Call Creation

extension NetworkAPI {
    func call<Value>(_ endpoint: Endpoint) -> AnyPublisher<Value, Error> where Value: Decodable {
        return Just(baseURL)
            .tryMap { baseURL in try endpoint.urlRequest(forBaseURL: baseURL) }
            .flatMap { request in
                session
                    .dataTaskPublisher(for: request)
                    .tryMap { data, response in
                        guard let statusCode = (response as? HTTPURLResponse)?.statusCode else { throw NetworkError.unexpectedResponse }
                        
                        switch statusCode {
                        case HTTPCodes.success:
                            return data
                        default:
                            throw NetworkError.httpCode(statusCode)
                        }
                    }
            }
            .decode(type: Value.self, decoder: jsonDecoder)
            .unwrapUnderlyingError()
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
}
