import Foundation

protocol EndpointParameter: Hashable, URLQueryItemConvertible, RawRepresentable where RawValue: CustomStringConvertible {
    var name: String { get }
}

// MARK: URLQueryItemConvertible

extension EndpointParameter {
    var queryItem: URLQueryItem {
        URLQueryItem(name: name, value: rawValue.description)
    }
}
