import Foundation
import Combine

protocol GitHubAPI {
    func loadSortedRepositories(forOrganization organization: String, beyondIndex index: Int?) -> AnyPublisher<Loadable<PartialSequence<[Repository]>>, Never>
}
