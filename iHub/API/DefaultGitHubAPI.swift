import Foundation
import Combine

struct DefaultGitHubAPI: GitHubAPI, NetworkAPI {
    
    // MARK: Properties
    
    let baseURL: String
    let session: URLSession
    let jsonDecoder: JSONDecoder
    
    // MARK: GitHubAPI
    
    /// Loads all repositores by trigerring a request for all pages up to and including the `nextPage`. The results are collected, merged and returned when all requests finished (either by loading it from the cache or by getting it from the server).
    func loadSortedRepositories(forOrganization organization: String, beyondIndex index: Int?) -> AnyPublisher<Loadable<PartialSequence<[Repository]>>, Never> {
        let nextPage = index.map { (($0 - 1) / Repositories.OrganizationEndpoint.pageSize) + 2 } ?? 1
        
        let endpointCalls: [AnyPublisher<[Repository], Error>] = (1...nextPage)
            .map { page in
                call(Repositories.OrganizationEndpoint(organization: organization, page: page))
            }
        
        return endpointCalls
            .publisher
            .flatMap { $0 }
            .collect()
            .map { results in
                let reachedLastPage = results.contains { $0.isEmpty }
                let repositories = results.flatMap { $0 }
                return .loaded(reachedLastPage ? .complete(repositories) : .partial(repositories))
            }
            .catch { error in Just(.failed(error)) }
            .prepend(.isLoading)
            .eraseToAnyPublisher()
    }
}

// MARK: - Repositories.OrganizationEndpoint

extension DefaultGitHubAPI {
    enum Repositories {
        struct OrganizationEndpoint: Endpoint {
            
            static let pageSize = 30
            
            // MARK: Properties
            
            let organization: String
            
            let method: HTTPMethod = .get
            var headers: [String : String]? = ["Accept": "application/vnd.github.v3+json"]
            var path: String { "/orgs/\(organization)/repos" }
            let parameters: [URLQueryItemConvertible]?
            let body: Data? = nil
            
            // MARK: Init
            
            init(organization: String, page: Int) {
                self.organization = organization
            
                parameters = [
                    SortParameter.fullName,
                    PageParameter(rawValue: page),
                    PerPageParameter(rawValue: Self.pageSize)
                ]
            }
        }
    }
}

// MARK: Parameters

extension DefaultGitHubAPI.Repositories.OrganizationEndpoint {
    enum SortParameter: String, EndpointParameter {
        case fullName = "full_name"
        
        var name: String { "sort" }
    }
    
    struct PageParameter: EndpointParameter {
        let rawValue: Int
        
        var name: String { "page" }
    }
    
    struct PerPageParameter: EndpointParameter {
        let rawValue: Int
        
        var name: String { "per_page" }
    }
}
