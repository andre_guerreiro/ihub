import Foundation

enum Loadable<Value> {
    case notRequested
    case isLoading
    case loaded(Value)
    case failed(Error)
}

// MARK: State Inspection

extension Loadable {
    var notRequested: Bool {
        guard case .notRequested = self else { return false }
        return true
    }
    
    var isLoading: Bool {
        guard case .isLoading = self else { return false }
        return true
    }
    
    var loaded: Bool {
        guard case .loaded = self else { return false }
        return true
    }
    
    var failed: Bool {
        guard case .failed = self else { return false }
        return true
    }
}

// MARK: Value Unwrapping

extension Loadable {
    var value: Value? {
        guard case let .loaded(value) = self else { return nil }
        return value
    }
    
    var error: Error? {
        guard case let .failed(error) = self else { return nil }
        return error
    }
}

// MARK: Higher Order Functions

extension Loadable {
    func map<MappedValue>(_ transform: (Value) throws -> MappedValue) rethrows -> Loadable<MappedValue> {
        do {
            switch self {
            case .notRequested:
                return .notRequested
            case .isLoading:
                return .isLoading
            case let .loaded(value):
                return .loaded(try transform(value))
            case let .failed(error):
                return .failed(error)
            }
        } catch {
            return .failed(error)
        }
    }
}

// MARK: Equatable

extension Loadable: Equatable where Value: Equatable {
    static func == (lhs: Loadable<Value>, rhs: Loadable<Value>) -> Bool {
        switch (lhs, rhs) {
        case (.notRequested, .notRequested),
            (.isLoading, .isLoading):
            return true
        case let (.loaded(lhsV), .loaded(rhsV)):
            return lhsV == rhsV
        case let (.failed(lhsE), .failed(rhsE)):
            return lhsE.localizedDescription == rhsE.localizedDescription
        default:
            return false
        }
    }
}
