import Foundation

protocol URLQueryItemConvertible {
    var queryItem: URLQueryItem { get }
}
