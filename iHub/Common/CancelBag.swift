import Combine

final class CancelBag: Cancellable {
    
    private var cancellables = Set<AnyCancellable>()
    
    public init() {}
    
    public func store(_ cancellable: AnyCancellable) {
        cancellable.store(in: &cancellables)
    }
    
    public func cancel() {
        cancellables.forEach { $0.cancel() }
        cancellables.removeAll()
    }
}

// MARK: -

extension AnyCancellable {
    func store(in cancelBag: CancelBag) {
        cancelBag.store(self)
    }
}

// MARK: -

extension Sequence where Element: AnyCancellable {
    func store(in cancelBag: CancelBag) {
        forEach { cancelBag.store($0) }
    }
}
