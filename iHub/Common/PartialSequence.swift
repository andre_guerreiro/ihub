import Foundation

enum PartialSequence<Value: Sequence> {
    case partial(Value)
    case complete(Value)
}

// MARK: State Inspection

extension PartialSequence {
    var isComplete: Bool {
        guard case .complete = self else { return false }
        return true
    }
}

// MARK: Value Unwrapping

extension PartialSequence {
    var value: Value {
        switch self {
        case .partial(let value), .complete(let value):
            return value
        }
    }
}

// MARK: Higher Order Functions

extension PartialSequence {
    func map<MappedValue>(_ transform: (Value.Element) throws -> MappedValue.Element) rethrows -> PartialSequence<MappedValue> where MappedValue: Sequence {
        switch self {
        case let .partial(value):
            return .partial(try value.map(transform) as! MappedValue)
        case let .complete(value):
            return .complete(try value.map(transform) as! MappedValue)
        }
    }
}

// MARK: Equatable

extension PartialSequence: Equatable where Value: Equatable {
    static func == (lhs: PartialSequence<Value>, rhs: PartialSequence<Value>) -> Bool {
        switch (lhs, rhs) {
        case let (.partial(lhsV), .partial(rhsV)),
            let (.complete(lhsV), .complete(rhsV)):
            return lhsV == rhsV
        default:
            return false
        }
    }
}
