import Combine

extension Publisher {
    func unwrappedValues<Value>() -> AnyPublisher<Value, Never> where Output == Loadable<Value>, Failure == Never {
        compactMap { loadable in
            guard case let .loaded(value) = loadable else { return nil }
            return value
        }
        .eraseToAnyPublisher()
    }
    
    func unwrappedErrors<Value>() -> AnyPublisher<Error, Never> where Output == Loadable<Value>, Failure == Never {
        compactMap { loadable in
            guard case let .failed(error) = loadable else { return nil }
            return error
        }
        .eraseToAnyPublisher()
    }
    
    func unwrappedLoadingStates<Value>() -> AnyPublisher<Bool, Never> where Output == Loadable<Value>, Failure == Never {
        map { loadable in loadable.isLoading }
        .removeDuplicates()
        .eraseToAnyPublisher()
    }
    
    func unwrappedRequestedStates<Value>() -> AnyPublisher<Bool, Never> where Output == Loadable<Value>, Failure == Never {
        map { loadable in !loadable.notRequested }
        .removeDuplicates()
        .eraseToAnyPublisher()
    }
}
