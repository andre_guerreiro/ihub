import Foundation

extension Sequence {
    func sorted<T: Comparable>(
        by keyPath: KeyPath<Element, T>,
        using comparator: (T, T) -> Bool = (<)
    )
        -> [Element]
    {
        sorted { comparator($0[keyPath: keyPath], $1[keyPath: keyPath]) }
    }
}
