import Foundation
import Combine

extension Publisher {
    func unwrapUnderlyingError() -> Publishers.MapError<Self, Failure> {
        mapError { error in (error.underlyingError as? Failure) ?? error }
    }
}

private extension Error {
    var underlyingError: Error? { (self as NSError).userInfo[NSUnderlyingErrorKey] as? Error }
}
